#include "Utils.h"

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>


std::string Utils::loadFromFile(std::string path)
{
	std::ifstream fileContent;
	fileContent.exceptions(std::ifstream::badbit);
	try
	{
		fileContent.open(path);
		std::stringstream fileStream;
		//Read file.
		fileStream << fileContent.rdbuf();
		//Close file.
		fileContent.close();
		//Return string.
		return fileStream.str();
	}
	catch(std::ifstream::failure e)
	{
		std::cout << "File:" << path << "wasn't successfully read." << std::endl;
		return NULL;
	}
}