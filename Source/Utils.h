#ifndef UTILS_H
#define UTILS_H

#include <iostream>

class Utils
{
public:
	static std::string loadFromFile(std::string path);
};

#endif