#ifndef EDITOR_H
#define EDITOR_H

#include "Display.h"

#define GLEW_STATIC
#include <GL/glew.h>

#include <GLFW/glfw3.h>

class Editor
{
private:
	Display display;
	static void keyCallBack(GLFWwindow* window, int key, int scancode, int action, int mode);
public:
    Editor();
    void loop();
};

#endif // EDITOR_H
