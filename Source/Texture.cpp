#include "Texture.h"
#include "Utils.h"

#include <iostream>

#define GLEW_STATIC
#include <GL/glew.h>
#include <SOIL.h>
/*
	Default constuctor.
*/
Texture::Texture(const char* texturePath)
{
	createTexture(texturePath);
}

/*
	There is nothing special to say about this :)
	Its called everytime when Texture is created.
*/
void Texture::createTexture(const char* texturePath)
{
	unsigned char* image = SOIL_load_image(texturePath, &m_width, &m_height, 0, SOIL_LOAD_RGB);

	glGenTextures(1, &m_texture);
	glBindTexture(GL_TEXTURE_2D, m_texture);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_width, m_height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);
	
	SOIL_free_image_data(image);
	glBindTexture(GL_TEXTURE_2D, 0);

}

/*
	Everytime you have to use texture for your object, 
	be sure to use this method.
*/
void Texture::bind()
{
	glBindTexture(GL_TEXTURE_2D, m_texture);
}