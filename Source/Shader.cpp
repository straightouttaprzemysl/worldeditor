#include "Shader.h"
#include "Utils.h"

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <string>

Shader::Shader(const GLchar* vertexPath, const GLchar* fragmentPath)
{
	loadShader(shaderVertex, GL_VERTEX_SHADER, Utils::loadFromFile(vertexPath).c_str());
	loadShader(shaderFragment, GL_FRAGMENT_SHADER, Utils::loadFromFile(fragmentPath).c_str());
	createProgram();
	std::cout << shaderProgram << std::endl;
}

void Shader::loadShader(GLuint& shader, GLenum shaderType, const GLchar* shaderSource)
{
	GLchar compilationInfo[512];
	GLint compilationSuccess;

	shader = glCreateShader(shaderType);
	glShaderSource(shader, 1, &shaderSource, NULL);
	glCompileShader(shader);
	//Check for errors
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compilationSuccess);
	if(!compilationSuccess)
	{
		glGetShaderInfoLog(shader, 512, NULL, compilationInfo);
		std::cout << "SHADER::COMPILATION::FAILED" << compilationInfo << std::endl;
	}
}

void Shader::createProgram()
{
	GLchar compilationInfo[512];
	GLint compilationSuccess;

	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, shaderVertex);
	glAttachShader(shaderProgram, shaderFragment);
	glLinkProgram(shaderProgram);
	//Check for errors
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &compilationSuccess);
	if(!compilationSuccess)
	{
		glGetProgramInfoLog(shaderProgram, 512, NULL, compilationInfo);
		std::cout << "SHADER::LINKING_FAILED" << compilationInfo << std::endl;
	}
	glDeleteShader(shaderVertex);
	glDeleteShader(shaderFragment);
}

void Shader::use()
{
	glUseProgram(shaderProgram);
}

GLuint Shader::getProgram()
{
	return shaderProgram;
}