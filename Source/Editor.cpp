#include "Editor.h"
#include "Shape.h"
#include "Shader.h"
#include "Display.h"

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>

Editor::Editor() : display(1024, 768, "WorldEditor")
{

}

void Editor::loop()
{
    GLfloat vertices[] = 
    {
    // Positions          // Colors           // Texture Coords
     0.0f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   0.5f, 1.0f,   // Top Right
     0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f,   // Bottom Right
    -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f   // Bottom Left
    };    
    
    Shader shader("Shaders/vertexShader.vs", "Shaders/fragmentShader.fs");
    Triangle triangle(vertices, sizeof(vertices), shader, "Textures/stalin.jpg");
    
    while(display.isWindowOpen())
    {
        glfwPollEvents();
		display.clear();

        triangle.shapeRender();

        glfwSwapBuffers(display.getWindow());
    }
}

int main()
{
	Editor editor;
    editor.loop();
}