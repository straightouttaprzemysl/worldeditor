#include "Display.h"

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>

Display::Display(int width, int height, const char* title) : window(width, height, title)
{
	initGLFW();
	initGLEW();
}

void Display::initGLFW()
{
	if (!glfwInit())
		std::cout << "Failed to initialize glfw." << std::endl;
	else
	{
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
		//Create window
		window.m_window = glfwCreateWindow(window.m_width, window.m_height, window.m_title, nullptr, nullptr);
		glfwMakeContextCurrent(window.m_window);
		//Here should be a keycallback but I moved it to Editor for a while.
	}
}

void Display::initGLEW()
{
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err)
		std::cout << "Failed to initialize GLEW.Error: " << glewGetErrorString(err) << std::endl;
	else
	{
		glViewport(0, 0, window.m_width, window.m_height);
	}
}

bool Display::isWindowOpen()
{
	return !glfwWindowShouldClose(window.m_window);
}

void Display::clear()
{
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
}

GLFWwindow* Display::getWindow()
{
	return window.m_window;
}