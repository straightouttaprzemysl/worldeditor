#ifndef SHAPE_H
#define SHAPE_H

#include "Shader.h"
#include "Texture.h"

#define GLEW_STATIC
#include <GL/glew.h>

class Triangle;

class Shape
{
protected:
	GLfloat* vertices;
	GLuint VBO, VAO;
	Shader shader;
	Texture texture;
public:
	Shape(GLfloat vertices[], Shader& shader, char* texturePath);
	virtual void shapeCreate();
	virtual void shapeRender();
};

class Triangle : public Shape
{
public:
	Triangle(GLfloat vertices[], size_t nFloats, Shader& shader, char* texturePath);

	void shapeCreate(size_t nFloats);
	void shapeRender();
};

#endif