#ifndef DISPLAY_H
#define DISPLAY_H

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

struct WindowRelated
{
	int m_width, m_height;
	GLFWwindow* m_window;
	const char* m_title;
	WindowRelated(int width, int height, const char* title) : m_height(height), m_width(width), m_title(title)
	{
	}
};

class Display
{
private:
	//Storage for window related variables.
	WindowRelated window;
	//This method is responsible for window initialization.
	void initGLFW();
	//Sets up viewport etc. 
	void initGLEW();
public:
	Display(int width, int height, const char* title);
	bool isWindowOpen();
	void clear();
	GLFWwindow* getWindow();
};

#endif
