#ifndef TEXTURE_H
#define TEXTURE_H

#define GLEW_STATIC
#include <GL/glew.h>

class Texture
{
private:
	GLuint m_texture;
	int m_width, m_height;
	void createTexture(const char* texturePath);
public:
	Texture(const char* texturePath);
	void bind();
};

#endif 