#include "Shape.h"

#define GLEW_STATIC
#include <GL/glew.h>
#include <SOIL.h>

#include <iostream>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

//   SHAPE   //

Shape::Shape(GLfloat vertices[], Shader& shader, char* texturePath) : shader(shader), vertices(vertices), texture(texturePath)
{
	
}

void Shape::shapeCreate()
{

}

void Shape::shapeRender()
{

}


//   Triangle   //

Triangle::Triangle(GLfloat vertices[], size_t nFloats, Shader& shader,
	char* texturePath ) : Shape(vertices, shader, texturePath)
{
	shapeCreate(nFloats);
	shader.use();
}

void Triangle::shapeCreate(size_t nFloats)
{
 	glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    // Bind the Vertex Array Object first, then bind and set vertex buffer(s) and attribute pointer(s).
    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, nFloats * sizeof(vertices), vertices, GL_STATIC_DRAW);
    //Position attrib
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);
	//Color attrib
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);
    // TexCoord attribute
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);

    glBindBuffer(GL_ARRAY_BUFFER, 0); // Note that this is allowed, the call to glVertexAttribPointer registered VBO as the currently bound vertex buffer object so afterwards we can safely unbind

    glBindVertexArray(0); // Unbind VAO (it's always a good thing to unbind any buffer/array to prevent strange bugs)
}

void Triangle::shapeRender()
{
	texture.bind();
	shader.use();
	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, 3);
	glBindVertexArray(0);
}