#ifndef SHADER_H
#define SHADER_H

#define GLEW_STATIC
#include <GL/glew.h>

class Shader
{
private:
	GLuint shaderProgram, shaderVertex, shaderFragment;
	
public:
	Shader(const GLchar* vertexPath, const GLchar* fragmentPath);
	void loadShader(GLuint& shader, GLenum shaderType, const GLchar* shaderSource);
	void createProgram();
	void use();
	GLuint getProgram();
};

#endif